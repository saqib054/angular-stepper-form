import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";

import { RecaptchaModule } from "ng-recaptcha";

import {
  MatButtonModule,
  MatStepperModule,
  MatInputModule,
  MatSnackBarModule,
} from "@angular/material";
import { AppComponent } from "./app.component";
import { RegistrationStep1Component } from "./components/registration-step1/registration-step1.component";
import { RegistrationStep2Component } from "./components/registration-step2/registration-step2.component";
import { RegistrationStep3Component } from "./components/registration-step3/registration-step3.component";
import { RegisterationService } from "./components/registeration.service";

@NgModule({
  declarations: [
    AppComponent,
    RegistrationStep1Component,
    RegistrationStep2Component,
    RegistrationStep3Component,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatStepperModule,
    MatInputModule,
    HttpClientModule,
    MatSnackBarModule,
    RecaptchaModule.forRoot(),
  ],
  providers: [RegisterationService],
  bootstrap: [AppComponent],
})
export class AppModule {}

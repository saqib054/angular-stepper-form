import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  title = `Multi Step Form Angular`;

  registrationForm: FormGroup;

  ngOnInit(): void {
    this.registrationForm = new FormGroup({
      personalDetails: new FormGroup({
        firstName: new FormControl(null, [
          Validators.required,
          Validators.minLength(3),
        ]),
        lastName: new FormControl(null),
        phone: new FormControl(null, [
          Validators.required,
          Validators.pattern(/^\d{10}$/),
        ]),
        email: new FormControl(null, [Validators.required, Validators.email]),
        gender: new FormControl(null, Validators.required),
        address: new FormControl(null),
      }),
      queryDetails: new FormGroup({
        customerQuery: new FormControl(null),
      }),
    });
  }
}

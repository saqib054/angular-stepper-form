import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class RegisterationService {
  apiUrl = "http://localhost:5000/api/";

  constructor(private http: HttpClient) {}

  // create customer
  createCustomer(formValue) {
    return this.http.post(this.apiUrl + "customer", formValue);
  }
}

import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { RegisterationService } from "../registeration.service";
import { throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-registration-step3",
  templateUrl: "./registration-step3.component.html",
  styleUrls: ["./registration-step3.component.css"],
})
export class RegistrationStep3Component implements OnInit {
  constructor(
    private registerationService: RegisterationService,
    public snackBar: MatSnackBar
  ) {}

  @Input() regForm: FormGroup;
  @Input() stepper;
  formSubmitted: boolean = false;
  isLoading: boolean = false;
  validationErrors = [];
  captchaResponse;

  ngOnInit() {}

  clearErrors() {
    if (this.validationErrors.length) {
      this.validationErrors = [];
    }
  }

  resetForm() {
    this.clearErrors();
    this.stepper.reset();
  }

  resolved(captchaResponse) {
    this.captchaResponse = captchaResponse;
  }

  submit() {
    this.isLoading = true;
    const formData = {
      ...this.regForm.value.personalDetails,
      ...this.regForm.value.queryDetails,
      captcha: this.captchaResponse,
    };
    this.registerationService.createCustomer(formData);
    this.registerationService
      .createCustomer(formData)
      .pipe(
        catchError((err) => {
          const error = err.error.validationErrors
            ? "Please fix validation errors"
            : err.error.message
            ? err.error.message
            : "Something bad happend, Try refreshing page";
          this.validationErrors = err.error.validationErrors
            ? err.error.validationErrors
            : [];
          this.snackBar.open(error, null, {
            duration: 5000,
          });
          this.isLoading = false;
          this.captchaResponse = null;
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000,
        });
        this.isLoading = false;
        this.formSubmitted = true;
        this.regForm.reset();
        this.stepper.reset();
        this.captchaResponse = null;
      });
  }
}

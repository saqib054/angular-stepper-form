# Getting Started

Multi-step angular form application. It is build with Angular and Twitter Bootstrap.

## Get the Code

```
git clone https://saqib054@bitbucket.org/saqib054/angular-stepper-form.git
cd angular-stepper-form
npm install
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
